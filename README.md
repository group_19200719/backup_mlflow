# Описание

Содержит python-package с функциями для создания бекапа для MlFlow развернутого по сценарию 4 описанному в офф. документации ([ссылка](https://mlflow.org/docs/2.3.2/tracking.html#scenario-4-mlflow-with-remote-tracking-server-backend-and-artifact-stores)).

Функции осуществляют бекап:
* базы данных PostresSQL
* бакета S3 Minio

Мы разворачиваем систему микросервисов `MlFlow` (`postgress`, `pgadmin`, `minio`, `mlflow`)
с помощью `docker-compose` согласно описанию в репозитории `emotion_recognize` в разделе `emotion_recognize/Docker` ([ссылка](https://gitlab.com/group_19200719/emotion_recognize/-/tree/master/Docker))