import os
from glob import glob
from prefect import task


@task(log_prints=True)
def stop_mlflow(mlflow_dir):
    os.chdir(mlflow_dir)
    print("Приостанавливаем MlFlow docker-composed контейнеры")
    mlflow_stop_cmd = (
        "docker-compose --env-file=.env -f docker-compose.yaml stop"
    )

    sys_code = os.system(mlflow_stop_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def stop_postgres_container(container_name):
    stop_container_cmd = f"docker stop {container_name}"

    sys_code = os.system(stop_container_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def print_backup_files(cvat_backup_path):
    print("Файлы бекапа созданы:")

    cmd = f"ls {cvat_backup_path}"
    sys_code = os.system(cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def start_postgres_container(container_name):
    start_container_cmd = f"docker start {container_name}"

    sys_code = os.system(start_container_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def restart_mlflow(mlflow_dir):
    os.chdir(mlflow_dir)
    print("Запускаем MlFlow docker-composed контейнеры")

    mlflow_restart_cmd = (
        "docker-compose --env-file=.env -f docker-compose.yaml start"
    )

    sys_code = os.system(mlflow_restart_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_mlflow_db(backup_folder, cur_time, postgres_user, postgres_db):
    os.chdir(backup_folder)

    print("Создаю бекап БД MlFlow")
    mlflow_backup_cmd = (
        f"docker exec "
        f"-i pg_container "
        f"pg_dump -U {postgres_user} "
        f"{postgres_db} > {cur_time}_{postgres_db}.sql"
    )

    sys_code = os.system(mlflow_backup_cmd)


@task(log_prints=True)
def backup_minio_s3_data(backup_folder, minio_s3_bucket_folder, cur_time):
    bucket_name = os.path.split(minio_s3_bucket_folder)[-1]

    os.chdir(backup_folder)
    print("Создаю бекап S3 minio бакета")

    arhive_name = f"{cur_time}_{bucket_name}.tar.gz"
    backup_minio_s3_data_cmd = f"tar -czvf  {arhive_name} {minio_s3_bucket_folder}"

    sys_code = os.system(backup_minio_s3_data_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def copy_minio_s3_folder(minio_data, minio_backup_dir, minio_bucket_name, cur_time):
    print("Создаю копию S3 minio бакета")
    source = os.path.join(minio_data, minio_bucket_name)
    destination = os.path.join(minio_backup_dir, minio_bucket_name)

    copy_minio_bucket_cmd = f"cp -r {source} {destination}"
    sys_code = os.system(copy_minio_bucket_cmd)
    if sys_code:
        raise ValueError

    create_datetime_txt_file(cur_time, destination)


# @task(log_prints=True)
def create_datetime_txt_file(cur_time, path2folder):
    data_txt_path = os.path.join(path2folder, f"{cur_time}.txt")
    cmd_create_data_txt = f"touch {data_txt_path}"
    sys_code = os.system(cmd_create_data_txt)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def restore_minio_s3_folder(backup_folder, minio_s3_bucket_folder):
    minio_dir, bucket_name = os.path.split(minio_s3_bucket_folder)
    backup_dir = os.path.join(backup_folder, bucket_name)

    # удалим файл с датой
    search_mask = os.path.join(backup_dir, "*.txt")
    data_txt_path = glob(search_mask)[0]
    os.remove(data_txt_path)
    backup_date = os.path.splitext(data_txt_path)[0]
    backup_date = os.path.split(backup_date)[-1]
    print(f"Восстанавливаем данные из бекапа {backup_date}")

    copy_minio_s3_data_cmd = f"rsync -r {backup_dir}/ {minio_s3_bucket_folder}"
    sys_code = os.system(copy_minio_s3_data_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def restore_mlflow_db(backup_folder, postgres_user, postgres_db):
    os.chdir(backup_folder)

    search_mask = os.path.join(backup_folder, "*.sql")
    backup_postgres_db = glob(search_mask)[0]
    backup_postgres_db = os.path.split(backup_postgres_db)[-1]

    print("удаляем текущую БД MlFlow в контейнере")
    mlflow_backup_cmd = (
        f"yes |docker exec "
        f"-i pg_container "
        f"dropdb -U {postgres_user} "
        f"-i -e -f {postgres_db}"
    )
    sys_code = os.system(mlflow_backup_cmd)

    print("Создаем пустую БД MlFlow в контейнере")
    mlflow_backup_cmd = (
        f"docker exec -i pg_container createdb -U {postgres_user} {postgres_db}"
    )
    sys_code = os.system(mlflow_backup_cmd)

    print("Восстанавливаем БД MlFlow в контейнере")
    mlflow_backup_cmd = (
        f"docker exec "
        f"-i pg_container "
        f"psql -U {postgres_user} "
        f"-d {postgres_db} < {backup_postgres_db}"
    )
    sys_code = os.system(mlflow_backup_cmd)
